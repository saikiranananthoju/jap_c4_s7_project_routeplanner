package com.routeplanner.standard;

import com.routeplanner.entity.Route;
import com.routeplanner.exception.NoRouteFoundException;

import java.io.IOException;

public interface IRouteService {

    Route[] findDirectFlightsFromLocation(String from) throws NoRouteFoundException, IOException;

    Route[] findFlights(String from,String to) throws NoRouteFoundException, IOException;
}
