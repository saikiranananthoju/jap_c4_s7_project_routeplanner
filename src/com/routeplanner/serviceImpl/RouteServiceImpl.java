package com.routeplanner.serviceImpl;

import com.routeplanner.entity.Route;
import com.routeplanner.exception.NoRouteFoundException;
import com.routeplanner.standard.IRouteService;

import java.io.IOException;

public class RouteServiceImpl implements IRouteService {
    RouteDaoServiceImpl routeDaoService = new RouteDaoServiceImpl();
    Route[] routesarray = null;

    @Override
    public Route[] findDirectFlightsFromLocation(String from) throws NoRouteFoundException, IOException {
        routeDaoService.fetchDataFromCsv();
        routesarray = routeDaoService.getRoutes();
        Route[] foundedroute = new Route[5];
            int index = 0;
            for (Route route : routesarray) {
                if (route.getFrom().equalsIgnoreCase(from)) {
                    foundedroute[index] = route;
                    index++;
                }
            }
            try {
                if (foundedroute[0] == null) {
                    throw new NoRouteFoundException("No Flights From Location " + from);
                }
            }catch (NoRouteFoundException e){
                System.out.println(e.getMessage());
            }
        return foundedroute;
    }

    @Override
    public Route[] findFlights(String from, String to) throws NoRouteFoundException, IOException {
        routeDaoService.fetchDataFromCsv();
        routesarray = routeDaoService.getRoutes();
        Route[] foundedRoute = new Route[5];
        int index = 0;
        for (Route route : routesarray) {
            if (route.getFrom().equalsIgnoreCase(from) && route.getTo().equalsIgnoreCase(to)) {
                foundedRoute[index] = route;
                index++;
            }
        }
        try {
            if (foundedRoute[0] == null) {
                throw new NoRouteFoundException("No Flights From Location " + from +" to " + to);
            }
        } catch (NoRouteFoundException e) {
            System.out.println(e.getMessage());
        }
        return foundedRoute;
    }

}
