package com.routeplanner.frontend;

import java.io.IOException;

public class FrontendMain {

    public static void main(String[] args) throws IOException {
        Display display = new Display();
        display.excute();
    }
}
