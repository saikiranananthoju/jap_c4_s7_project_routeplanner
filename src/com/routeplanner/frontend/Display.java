package com.routeplanner.frontend;

import com.routeplanner.entity.Route;
import com.routeplanner.serviceImpl.RouteDaoServiceImpl;
import com.routeplanner.serviceImpl.RouteServiceImpl;

import java.io.IOException;

public class Display {
    public void excute() throws IOException {
        Display frontendMain = new Display();
        RouteServiceImpl routeService = new RouteServiceImpl();
        RouteDaoServiceImpl routeDaoService = new RouteDaoServiceImpl();
        System.out.println("-------------------findDirectFlights-------------------");
        frontendMain.todisplay( routeService.findDirectFlightsFromLocation("Delhi"));
        Route[] foundedflight =  routeService.findDirectFlightsFromLocation("surat");
        frontendMain.todisplay(foundedflight);
        System.out.println("-------------------findDFlights------------------------");
        Route[] foundedflightfromto = routeService.findFlights("Frankfurt","London");
        frontendMain.todisplay(routeService.findFlights("surat","nandurbar"));
        frontendMain.todisplay(foundedflightfromto);
        System.out.println("-------------------findall------------------------");
        routeDaoService.fetchDataFromCsv();
        Route[] founded = routeDaoService.findall();
        frontendMain.todisplay(founded);


    }
    public void todisplay(Route[] routes){
        int index = 1;
        for(Route route:routes){

            if(route!=null) {
                System.out.println(index+" "+route.getFrom() + " " + route.getTo() + " " + route.getDistance() + " " + route.getTravelTime() + " " + route.getAirfare());
                index++;
            }
        }
    }

}
